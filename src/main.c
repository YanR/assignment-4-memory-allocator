#include "assert.h"
#include "mem.h"
#include "mem_internals.h"

#define MAP_ANONYMOUS 0x20

struct block_header* get_header(void *content) {
    return (struct block_header*) (content-offsetof(struct block_header, contents));
}

void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

int success_memalloc() {
    heap_init(0);
    int* array = _malloc(sizeof (int) * 10);
    struct block_header* blockHeader = get_header(array);
    assert(!blockHeader->is_free);
    assert(blockHeader->capacity.bytes == 40);
    assert(blockHeader->next != NULL);
    heap_term();
    return 0;
}

int free_one() {
    heap_init(0);
    int* block1 = _malloc(sizeof (int) * 10);
    int* block2 = _malloc(sizeof (int) * 20);
    int* block3 = _malloc(sizeof (int) * 30);
    _free(block2);
    struct block_header* header1 = get_header(block1);
    struct block_header* header2 = get_header(block2);
    struct block_header* header3 = get_header(block3);
    assert(header1->is_free == false);
    assert(header1->next == header2);
    assert(header2->is_free);
    assert(header2->next == header3);
    heap_term();
    return 0;
}

int free_two() {
    heap_init(0);
    int* block1 = _malloc(sizeof (int) * 10);
    int* block2 = _malloc(sizeof (int) * 20);
    int* block3 = _malloc(sizeof (int) * 30);
    _free(block2);
    struct block_header* header1 = get_header(block1);
    struct block_header* header2 = get_header(block2);
    struct block_header* header3 = get_header(block3);
    size_t cap1 = header1->capacity.bytes;
    size_t cap2 = header2->capacity.bytes;
    assert(header1->is_free == false);
    assert(header1->next == header2);
    _free(block1);
    assert(header1->next == header3);
    assert(header1->is_free);
    assert(header1->capacity.bytes == offsetof(struct block_header, contents) + cap1 + cap2);
    heap_term();
    return 0;
}

int test_grow_heap() {
    heap_init(0);
    void *block1 = _malloc(8000);
    struct block_header* header1 = get_header(block1);
    struct block_header* next = header1->next;
    void *block2 = _malloc(1000);
    struct block_header* header2 = get_header(block2);
    assert(header1->next == header2);
    assert(header2 == next);
    assert(header2->next->capacity.bytes == REGION_MIN_SIZE + (REGION_MIN_SIZE - 8000) - 1000 - offsetof(struct block_header, contents) * 3);
    heap_term();
    return 0;
}

int test_grow_heap_different_range() {
    void *block0 = mmap(HEAP_START + REGION_MIN_SIZE, 1024, 0, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    assert(block0 != NULL);
    heap_init(0);
    void *block = _malloc(REGION_MIN_SIZE - offsetof(struct block_header, contents));
    void *block2 = _malloc(REGION_MIN_SIZE - offsetof(struct block_header, contents));
    struct block_header* header1 = get_header(block);
    struct block_header* header2 = get_header(block2);
    assert(header1->next == header2);
    assert((void*) header1->contents + header1->capacity.bytes != header2);
    assert((void*) header1->contents + header1->capacity.bytes == block0);
    munmap(block0, 1024);
    heap_term();
    return 0;
}

int main() {
    success_memalloc();
    free_one();
    free_two();
    test_grow_heap();
    test_grow_heap_different_range();
    return 0;
}