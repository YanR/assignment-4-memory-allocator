#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }

static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    if (addr == NULL) {
        return;
    }
    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

extern inline bool region_is_invalid(const struct region *r);


static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query) {
    struct region region;
    region.extends = true;
    if (query < REGION_MIN_SIZE) {
        query = REGION_MIN_SIZE;
    } else if (query == REGION_MIN_SIZE) {
        query = (size_t) (REGION_MIN_SIZE + getpagesize());
    }
    region.addr = map_pages(addr, query, MAP_FIXED_NOREPLACE);
    if (region.addr == MAP_FAILED) {
        region.addr = map_pages(addr, query, 0);
        region.extends = false;
    }
    if (region.addr == MAP_FAILED) {
        region.addr = NULL;
        region.extends = false;
    }
    region.size = query;
    block_init(region.addr, (block_size) {.bytes = region.size}, NULL);
    return region;
}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;

    return region.addr;
}

/*  освободить всю память, выделенную под кучу */
void heap_term() {
    struct block_header* blockHeader = (struct block_header*) HEAP_START;
    struct block_header* it = (struct block_header*) HEAP_START;
    struct block_header* last = (struct block_header*) HEAP_START;
    while (true) {
        blockHeader = (struct block_header*) HEAP_START;
        it = (struct block_header*) HEAP_START;

        while (it->next != NULL) {
            if (it->next != block_after(it)) {
                blockHeader = it->next;
                last = it;
            }
            it = it->next;
        }
        last->next = NULL;
        munmap(blockHeader, (void *) (it->capacity.bytes + it->contents) - (void *) blockHeader);
        if (blockHeader == HEAP_START) {
            break;
        }
    }
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free &&
           query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query) {
    if (block_splittable(block, query)) {
        struct block_header *blockNext = block->next;
        block_size blockSize = size_from_capacity((block_capacity) {.bytes = query});
        block_size blockMainSize = size_from_capacity(block->capacity);
        block_init(block, blockSize, blockSize.bytes + (void*) block);
        block_init(block->next, (block_size) {blockMainSize.bytes - blockSize.bytes}, blockNext);
        return true;
    }
    return false;
}


/*  --- Слияние соседних свободных блоков --- */

static void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous(
        struct block_header const *fst,
        struct block_header const *snd) {
    return (void *) snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block) {
    if (block != NULL && block->next != NULL && mergeable(block, block->next)) {
        block_init(block,
                   size_from_capacity((block_capacity) {
                       block->capacity.bytes + block->next->capacity.bytes + offsetof(struct block_header, contents)
                   }),
                   block->next->next);
        return true;
    }
    return false;
}

/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header *block;
};

static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
    struct block_header* it = block;
    sz = size_max(sz, BLOCK_MIN_CAPACITY);
    struct block_search_result result;
    while (it != NULL) {
        bool cont = true;
        while (it->next != NULL && it->next->is_free && cont) {
            cont = try_merge_with_next(it);
        }
        if (it->capacity.bytes >= sz && it->is_free) {
            result.type = BSR_FOUND_GOOD_BLOCK;
            result.block = it;
            return result;
        }
        if (it->next == NULL) {
            break;
        }
        it = it->next;
    }
    result.type = BSR_REACHED_END_NOT_FOUND;
    result.block = it;
    return result;
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    query = size_max(query, BLOCK_MIN_CAPACITY);
    struct block_search_result result = find_good_or_last(block, query);
    if (result.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(result.block, query);
        result.block->is_free = false;
    }
    return result;
}


static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
    if (last == NULL) {
        return 0;
    }
    void *block = alloc_region((void *) last + offsetof( struct block_header, contents ) + last->capacity.bytes, query).addr;
    last->next = block;
    if (try_merge_with_next(last)) {
        return last;
    }
    return (struct block_header*) block;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
    struct block_search_result result = try_memalloc_existing(query, heap_start);
    if (result.type == BSR_FOUND_GOOD_BLOCK) {
        return result.block;
    }
    size_t Q = query;
    if (Q % REGION_MIN_SIZE == 0) {
        Q += REGION_MIN_SIZE/2;
    } else {
        Q = Q - Q % REGION_MIN_SIZE + REGION_MIN_SIZE/2;
    }
    grow_heap(result.block, Q);
    result = try_memalloc_existing(query, heap_start);
    if (result.type == BSR_FOUND_GOOD_BLOCK) {
        return result.block;
    }
    return NULL;
}

void *_malloc(size_t query) {
    struct block_header *const addr = memalloc(query, (struct block_header *) HEAP_START);
    if (addr) return addr->contents;
    else return NULL;
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

void _free(void *mem) {
    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    struct block_header* it = header;
    bool cont = true;
    while (it->next != NULL && it->next->is_free && cont) {
        cont = try_merge_with_next(it);
    }
}
